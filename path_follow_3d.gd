extends PathFollow3D
var offset_amount = 0.0
var offset_increase = 4.0
var begin_decrease = 500.0
var decrease = false


# Called when the node enters the scene tree for the first time.
func _physics_process(delta):
	print("offset: " + str(offset))
	if not decrease:
		offset_amount += offset_increase * delta
		if offset > begin_decrease:
			decrease = true
	else:
		offset_amount -= offset_increase * delta
		if offset < begin_decrease:
			decrease = false
	offset += offset_amount * delta
